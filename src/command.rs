//! Command that is executed by the shell
use std::{fmt, io, process::ExitStatus};

use termion::cursor;

use crate::config;

/// A Command to be executed
#[derive(Debug, Default)]
pub struct Command {
    command: String,
    args: Vec<String>,
    cursor: u16,
}

impl From<&str> for Command {
    fn from(s: &str) -> Self {
        Self {
            command: s.into(),
            ..Default::default()
        }
    }
}

impl Command {
    pub fn set(&mut self, command: impl Into<String>) {
        let c = command.into();
        if c.len() >= u16::MAX as usize {
            eprintln!("Command can't be bigger than {} characters\r", u16::MAX)
        } else {
            self.command = c;
            self.cursor = 0;
        }
    }

    pub fn clear(&mut self) {
        std::mem::take(self);
        self.cursor = 0;
    }

    pub fn add(&mut self, c: char) {
        if self.command.len() >= u16::MAX as usize - 1 {
            eprintln!("Command can't be bigger than {} characters\r", u16::MAX)
        } else {
            let pos = self.command.len().saturating_sub(self.cursor.into());
            self.command.insert(pos, c);
        }
    }

    pub fn del(&mut self) {
        if self.cursor == 0 {
            self.command.pop();
        } else {
            let pos = self.command.len().saturating_sub(1 + self.cursor as usize);
            self.command.remove(pos);
        }
    }

    /// Move to the end of the command
    pub fn end(&mut self) {
        self.cursor = 0;
    }

    /// Move to the start of the command
    pub fn start(&mut self) {
        self.cursor = self.command.len() as u16;
    }

    /// Move the cursor by num words
    pub fn wd(&mut self, num: i16) {
        let mut words = self.command.chars().rev().enumerate().peekable();

        let mut word_starts = Vec::from([0]);
        while let Some((i, ch)) = words.next() {
            if ch != ' ' && matches!(words.peek(), Some((_, ' '))) {
                word_starts.push(i as u16 + 1)
            }
        }

        let i = word_starts.partition_point(|&x| x < self.cursor);

        let abs = num.unsigned_abs();
        match num.signum() {
            1 => {
                self.cursor = match word_starts.get(i.saturating_sub(abs as usize)) {
                    Some(&i) => i,
                    None => 0,
                }
            }
            -1 => {
                self.cursor = if word_starts.get(i).copied() == Some(self.cursor) {
                    word_starts.get(i + abs as usize).copied()
                } else {
                    word_starts.get(i).copied()
                }
                .unwrap_or(self.command.len() as u16)
            }
            _ => (),
        }
    }

    /// Move the cursor by num characters
    pub fn ch(&mut self, num: i16) {
        let abs = num.unsigned_abs();
        match num.signum() {
            1 => self.cursor = self.cursor.saturating_sub(abs),
            -1 => {
                if self.command.len() > self.cursor as usize {
                    self.cursor = self.cursor.saturating_add(abs)
                }
            }
            _ => (),
        }
    }

    pub fn text(&self) -> &str {
        &self.command
    }

    pub fn text_with_cursor(&self) -> String {
        let mut cmd = self.command.clone();
        cmd.insert(self.cursor_l() as usize, '|');
        cmd
    }

    pub fn cursor_l(&self) -> u16 {
        (self.command.len() as u16).saturating_sub(self.cursor)
    }

    pub fn is_empty(&self) -> bool {
        self.command.is_empty()
    }

    pub fn args(&self) -> &[String] {
        self.args.as_ref()
    }
}

impl Command {
    pub fn get_args(&mut self) {
        let mut split = self.command.trim_end().split(' ');
        let command = split.next().unwrap_or_default().into();
        self.args.extend(split.map(|s| s.to_owned()));
        self.command = command;
    }

    pub fn alias(&mut self, config: &config::Config) {
        self.get_args();

        if let Some((alias, mut alias_args)) = config.alias(&self.command) {
            self.command = alias;
            alias_args.append(&mut self.args);
            self.args = alias_args;
        };
    }

    /// run the command.
    pub fn execute(self) -> Result<ExitStatus, io::Error> {
        let mut cmd = std::process::Command::new(&self.command)
            .args(self.args)
            .spawn()?;

        cmd.wait()
    }
}

impl fmt::Display for Command {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.command)?;

        if self.cursor > 0 {
            write!(f, "{}", cursor::Left(self.cursor))?;
        }

        Ok(())
    }
}

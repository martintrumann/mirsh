mod command;
mod config;
mod history;
mod mode;

mod action;
mod builtins;
mod keymap;
use builtins::builtins;

mod completion;

#[cfg(test)]
mod tests;

use command::Command;
use completion::Completion;
pub use config::Config;
pub use history::History;

use std::{
    fmt,
    io::{self, Write},
    path::PathBuf,
};
use termion::{cursor::DetectCursorPos, event::Key, raw::RawTerminal};

pub struct Shell {
    command: Command,
    mode: &'static str,
    history: History,
    config: Config,
    completions: Vec<Completion>,
    cwd: PathBuf,
    pub run: bool,
}

impl Shell {
    pub fn clear(&mut self) {
        self.command.clear()
    }

    pub fn new(history: History, config: Config) -> Self {
        let mut completions = Vec::new();
        for comp in config.comp_sources.iter() {
            match Completion::new(comp) {
                Ok(c) => completions.push(c),
                Err(e) => eprintln!("completion {comp:?} error: {e}"),
            }
        }

        Self {
            command: Command::default(),
            mode: "insert",
            history,
            config,
            completions,
            cwd: std::env::current_dir().unwrap(),
            run: true,
        }
    }

    pub fn handle(
        &mut self,
        key: termion::event::Key,
        stdout: &mut RawTerminal<impl std::io::Write>,
    ) -> Result<bool, io::Error> {
        match key {
            Key::Esc => self.mode = "normal",
            Key::Ctrl('d') => {
                writeln!(stdout, "\r").unwrap();
                return Ok(false);
            }
            Key::Ctrl('c') => {
                self.clear();
                self.mode = "insert";
                writeln!(stdout, "\r").unwrap();
            }
            Key::Ctrl('l') => {
                write!(
                    stdout,
                    "{}{}",
                    termion::cursor::Goto(1, 1),
                    termion::clear::All
                )
                .unwrap();
            }
            Key::Char('\n') => {
                write!(stdout, "\r\n").unwrap();
                let mut command = std::mem::take(&mut self.command);
                if command.is_empty() {
                    return Ok(true);
                }

                self.history.add(&command.to_string())?;

                command.alias(&self.config);

                let res = if let Some(cmd) = builtins().get(command.text()) {
                    cmd(self, command.args());
                    Ok(true)
                } else {
                    stdout.suspend_raw_mode().unwrap();

                    let res = command.execute();

                    stdout.activate_raw_mode().unwrap();

                    if stdout.cursor_pos().unwrap().0 > 1 {
                        write!(
                            stdout,
                            "{}%{}\n\r",
                            termion::style::Invert,
                            termion::style::NoInvert
                        )
                        .unwrap();
                    }

                    res.map(|_| true)
                };

                self.mode = "insert";
                self.history.index = 0;

                return res;
            }
            Key::Char('\t') => {
                // WIP: Tab autocompletion
                for source in self.completions.iter_mut() {
                    write!(stdout, "\n\r{}:\r\n", source.name()).unwrap();

                    match source.complete(&self.command, &self.cwd) {
                        Ok(completion_items) => {
                            // TODO: Allow selecting completion items
                            write!(stdout, "{:?}\n\r", completion_items).unwrap();
                        }
                        Err(e) => {
                            stdout.suspend_raw_mode().unwrap();
                            write!(stdout, "{}", e).unwrap();
                            stdout.flush().unwrap();
                            stdout.activate_raw_mode().unwrap();
                        }
                    };
                }
            }
            k => {
                if let Some(&action) = self.config.keymap.get(self.mode, &k) {
                    action.act(self)
                }

                if self.mode == "insert" {
                    match k {
                        Key::Backspace => self.command.del(),
                        Key::Char(ref k) => self.command.add(*k),
                        _ => (),
                    }
                }
            }
        }

        Ok(true)
    }
}

impl fmt::Display for Shell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut prompt = self.config.prompt.clone();

        #[cfg(feature = "time")]
        {
            let time = chrono::Local::now();
            prompt = prompt.replace("%t", &time.format("%H:%M:%S").to_string());
        }

        if let Ok(home) = std::env::var("HOME") {
            prompt = prompt.replace("%p", &self.cwd.to_str().unwrap().replace(&home, "~"));
        } else {
            prompt = prompt.replace("%p", &self.cwd.display().to_string());
        }

        prompt = prompt.replace("%M", &self.config.modes.get(self.mode).unwrap().to_string());

        write!(f, "\r{}{}", prompt, self.command)?;

        Ok(())
    }
}

use std::{
    fs::{File, OpenOptions},
    io::{Read, Write},
};

pub struct History {
    file: Option<File>,
    vec: Vec<String>,
    pub index: usize,
}

impl History {
    pub fn try_from_file() -> Option<Self> {
        let mut cache = dirs::cache_dir()?;
        cache.push("mirsh_history");

        let mut file = OpenOptions::new()
            .write(true)
            .read(true)
            .append(false)
            .create(true)
            .open(&cache)
            .ok()?;

        let vec = if cache.is_file() {
            let mut vec = String::new();
            file.read_to_string(&mut vec).unwrap_or_default();

            vec.trim().split('\n').map(|i| i.to_owned()).collect()
        } else {
            Vec::new()
        };

        Some(Self {
            file: Some(file),
            vec,
            index: 0,
        })
    }

    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        match Self::try_from_file() {
            Some(out) => out,
            None => {
                eprintln!("Failed to create history file, single process history only.\r");

                Self {
                    file: None,
                    vec: Vec::new(),
                    index: 0,
                }
            }
        }
    }

    pub fn add(&mut self, entry: &str) -> Result<(), std::io::Error> {
        if self.vec.last().map(String::as_str) == Some(entry) {
            return Ok(());
        }

        self.vec.push(entry.to_string());

        if let Some(file) = &mut self.file {
            writeln!(file, "{}", entry)?;
        }

        Ok(())
    }

    pub fn prev(&mut self, i: u16) -> Option<&String> {
        if self.index.checked_add(i.try_into().unwrap()) < Some(self.vec.len()) {
            self.index += i as usize;
        } else {
            self.index = self.vec.len() - 1
        }

        self.vec.get(self.vec.len() - self.index)
    }

    pub fn next(&mut self, i: u16) -> Option<&String> {
        if self.index.checked_sub(i.try_into().unwrap()) > Some(0) {
            self.index -= i as usize
        } else {
            return None;
        }

        self.vec.get(self.vec.len() - self.index)
    }

    pub fn mv(&mut self, n: i16) -> Option<&String> {
        let abs = n.unsigned_abs();
        match n.signum() {
            -1 => self.prev(abs),
            1 => self.next(abs),
            _ => self.vec.get(self.index),
        }
    }

    /// Get a reference to the history's vec.
    pub fn get(&self) -> &[String] {
        self.vec.as_ref()
    }
}

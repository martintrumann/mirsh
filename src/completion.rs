use std::{
    io::{self, Read, Write},
    path::Path,
    process::{Child, ChildStderr, ChildStdin, Command, Stdio},
    sync::mpsc::{sync_channel, Receiver},
    thread,
    time::Duration,
};

pub struct Completion {
    cmd: String,
    child: Child,
    stdin: ChildStdin,
    stderr: ChildStderr,
    out: Receiver<String>,
}

impl Completion {
    pub fn new(cmd: &str) -> Result<Self, io::Error> {
        let mut child = Command::new(cmd)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .spawn()?;

        let stdin = child.stdin.take().unwrap();
        let stderr = child.stderr.take().unwrap();

        let (sender, out) = sync_channel(1);

        let mut stdout = child.stdout.take().unwrap();
        thread::spawn(move || {
            let mut buf = String::new();
            stdout.read_to_string(&mut buf).unwrap();
            sender.send(buf).unwrap();
        });

        Ok(Completion {
            cmd: cmd.to_owned(),
            child,
            stdin,
            stderr,
            out,
        })
    }

    pub fn complete(&mut self, cmd: &crate::Command, wd: &Path) -> Result<Vec<String>, String> {
        if matches!(self.child.try_wait(), Ok(Some(_))) {
            // start the proccess again if it has exited
            *self = Self::new(&self.cmd).unwrap();
        }

        if let Err(_e) = writeln!(self.stdin, "{}\n{}", cmd.text_with_cursor(), wd.display()) {
            let mut buf = String::new();
            self.stderr.read_to_string(&mut buf).unwrap();
            return Err(buf);
        };

        let out = self.out.recv_timeout(Duration::from_secs(2));

        // TODO: turn output into completion items

        println!("{out:?}\r");

        // buf.split("\n").map(str::to_owned).collect();
        Ok(vec![])
    }

    pub fn kill(&mut self) {
        let _ = self.child.kill();
    }

    pub fn name(&self) -> &str {
        self.cmd.as_ref()
    }
}

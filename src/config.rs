use std::collections::HashMap;
use termion::{color, event::Key};

use crate::{
    action::Action,
    keymap::Keymap,
    mode::{Cursor, Mode},
};

pub struct Config {
    pub prompt: String,
    pub aliases: HashMap<String, String>,
    pub comp_sources: Vec<String>,
    pub modes: HashMap<&'static str, Mode>,
    pub keymap: Keymap,
}

impl Default for Config {
    fn default() -> Self {
        Self::new("%p %M ".into())
    }
}

impl Config {
    pub fn new(mut prompt: String) -> Self {
        #[cfg(not(feature = "time"))]
        {
            prompt = prompt.replace("%t ", "");
            prompt = prompt.replace("%t", "");
        }

        prompt = prompt
            .split('|')
            .map(|s| match s {
                "yellow" => format!("{}", color::Fg(color::Yellow)),
                "red" => format!("{}", color::Fg(color::Red)),
                "green" => format!("{}", color::Fg(color::Green)),
                "blue" => format!("{}", color::Fg(color::Blue)),
                "white" => format!("{}", color::Fg(color::White)),
                "reset" => format!("{}", color::Fg(color::Reset)),
                a => a.into(),
            })
            .collect();

        prompt.push_str(&format!("{}", color::Fg(color::Reset)));

        Self {
            prompt,
            aliases: HashMap::new(),
            comp_sources: Default::default(),
            modes: HashMap::from([
                (
                    "normal",
                    Mode {
                        letter: 'N',
                        colors: format!("{}", color::Bg(color::Blue)),
                        cursor: Cursor::Block,
                    },
                ),
                (
                    "insert",
                    Mode {
                        letter: 'I',
                        colors: format!("{}", color::Bg(color::Green)),
                        cursor: Cursor::Bar,
                    },
                ),
                (
                    "select",
                    Mode {
                        letter: 'S',
                        colors: format!("{}", color::Bg(color::Yellow)),
                        cursor: Cursor::Bar,
                    },
                ),
            ]),
            keymap: HashMap::from([(
                "normal",
                HashMap::from([
                    (Key::Char('h'), Action::Move(-1)),
                    (Key::Char('l'), Action::Move(1)),
                    (Key::Char('j'), Action::History(1)),
                    (Key::Char('k'), Action::History(-1)),
                    (Key::Char('i'), Action::Insert),
                ]),
            )])
            .into(),
        }
    }

    pub fn from_xdg_config() -> Self {
        let config_file = dirs::config_dir()
            .map(|d| d.join("mirsh/config"))
            .filter(|f| f.is_file());

        if let Some(config_file) = config_file {
            let config = std::fs::read_to_string(config_file).unwrap();
            let mut iter = config.split('\n');

            let mut config;
            if let Some(prompt) = iter.next() {
                config = Self::new(prompt.to_string());
            } else {
                config = Self::default();
            }

            config.parse_lines(iter.collect());

            config
        } else {
            Self::default()
        }
    }

    pub fn parse_lines(&mut self, lines: Vec<&str>) {
        let mut lines = lines.into_iter();
        let mut line_num = 0;
        while let Some(line) = lines.next() {
            line_num += 1;

            if line.starts_with(['"', '#']) || line.is_empty() {
                continue;
            } else if line.starts_with("include ") {
                let config_file = dirs::config_dir()
                    .unwrap()
                    .join(["mirsh", line.split_at(8).1].join("/"));

                match std::fs::read_to_string(&config_file) {
                    Ok(s) => self.parse_lines(s.split('\n').collect()),
                    Err(e) => {
                        eprintln!("Can't include {}: {}\r", config_file.display(), e)
                    }
                }
            } else if let Some(cmd) = line.strip_prefix("comp ") {
                self.comp_sources.push(cmd.to_owned())
            } else if line.contains('{') {
                self.keymap.parse_group(line, &mut lines, &mut line_num);
            } else if let Some((key, val)) = line.split_once(':') {
                let (val, _comment) = val
                    .split_once('#')
                    .map(|s| (s.0, Some(s.1)))
                    .unwrap_or((val, None));

                self.aliases.insert(key.to_string(), val.trim().to_string());
            } else {
                eprintln!("Failed to read line {}: {}\r", line_num, line);
            }
        }
    }

    pub fn alias(&self, command: &str) -> Option<(String, Vec<String>)> {
        self.aliases.get(command).map(|alias| {
            let (alias, mut args) = alias.split_once(' ').map_or_else(
                || (alias.clone(), Vec::new()),
                |(a, r)| (a.into(), r.split_whitespace().map(|s| s.into()).collect()),
            );

            if alias != command {
                if let Some((alias, mut new_args)) = self.alias(&alias) {
                    new_args.append(&mut args);
                    return (alias, new_args);
                }
            }

            (alias, args)
        })
    }
}

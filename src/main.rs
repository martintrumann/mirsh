use std::io::{stdin, stdout, Write};
use termion::{input::TermRead, raw::IntoRawMode};

use mirsh::{Config, History, Shell};

fn main() {
    ctrlc::set_handler(|| {}).expect("Can't set ctrlc");

    let history = History::new();

    let stdin = stdin();
    let stdout = stdout();
    let mut stdout = stdout.lock().into_raw_mode().unwrap();

    let mut shell = Shell::new(history, Config::from_xdg_config());

    write!(stdout, "{}", shell).unwrap();
    stdout.flush().unwrap();

    for b in stdin.keys() {
        let b = if let Ok(b) = b { b } else { continue };
        write!(stdout, "{}", shell).unwrap();

        match shell.handle(b, &mut stdout) {
            Ok(true) => (),
            Ok(false) => break,
            Err(e) => {
                shell.clear();
                write!(stdout, "{}\n\r", e).unwrap();
                stdout.flush().unwrap();
            }
        };

        if !shell.run {
            break;
        }

        write!(stdout, "{}{}", termion::clear::CurrentLine, shell).unwrap();
        stdout.flush().unwrap();
    }

    write!(stdout, "{}", termion::cursor::SteadyBlock).unwrap();
}

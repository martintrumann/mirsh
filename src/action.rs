//! Abstraction across the line actions so they could be keymapped

use std::str::FromStr;

use crate::Shell;

#[derive(Debug, Clone, Copy)]
pub enum Action {
    History(i16),
    Move(i16),
    Word(i16),
    WordEnd(i16),

    DeleteWord,

    Till(char),
    Find(char),

    Start,
    End,

    Insert,
    Append,
    AppendToEnd,
    Select,

    Delete,
    Change,
    Replace(char),
}

impl Action {
    pub fn act(&self, s: &mut Shell) {
        match *self {
            Action::History(n) => {
                if let Some(h) = s.history.mv(n) {
                    s.command.set(h)
                } else {
                    s.command.clear()
                }
            }

            Action::Move(i) => s.command.ch(i),
            Action::Word(i) => s.command.wd(i),

            Action::Start => s.command.start(),
            Action::End => s.command.end(),

            Action::Insert => s.mode = "insert",

            Action::Append => {
                s.command.ch(1);
                s.mode = "insert";
            }
            Action::AppendToEnd => {
                s.command.end();
                s.mode = "insert";
            }

            _ => (),
        }
    }
}

#[derive(Debug)]
pub enum Error {
    NotAnAction,
}

impl FromStr for Action {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let action = match s {
            "history_up" => Self::History(1),
            "history_down" => Self::History(-1),

            "move_left" => Self::Move(-1),
            "move_right" => Self::Move(1),

            "word_left" => Self::Word(-1),
            "word_right" => Self::Word(1),

            "line_start" => Self::Start,
            "line_end" => Self::End,

            _ => return Err(Error::NotAnAction),
        };

        Ok(action)
    }
}

use std::collections::HashMap;

use termion::event::Key;

use crate::action::{self, Action};

// TODO: Support mapping multiple keys
pub struct Keymap(HashMap<&'static str, HashMap<Key, Action>>);

impl From<HashMap<&'static str, HashMap<Key, Action>>> for Keymap {
    fn from(map: HashMap<&'static str, HashMap<Key, Action>>) -> Self {
        Self(map)
    }
}

impl Keymap {
    pub fn mode_exists(&self, mode: &str) -> bool {
        self.0.contains_key(mode)
    }

    pub fn get(&self, mode: &str, key: &Key) -> Option<&Action> {
        self.0.get(mode)?.get(key)
    }

    pub fn remove(&mut self, mode: &str, key: &Key) -> Option<()> {
        let mode = self.0.get_mut(mode)?;
        mode.remove(key).map(|_| ())
    }

    pub fn map(&mut self, mode: &str, key: Key, action: Action) -> Option<()> {
        if mode == "insert" && matches!(key, Key::Char(_) | Key::Backspace) {
            eprintln!("Can't rebind normal keys in insert mode\r");
            return None;
        }

        let mode = self.0.get_mut(mode)?;
        mode.insert(key, action);

        Some(())
    }

    pub fn parse_line(&mut self, mode: &str, line: &str) -> Result<(), &str> {
        if line.is_empty() || line.starts_with(['#', '"']) {
            return Ok(());
        }

        let (key, action) = match line.split_once(' ') {
            Some((key, a)) => (key.trim(), a.trim()),
            None => {
                return Err("no action");
            }
        };

        let key = if key.len() > 1 {
            return Err("TODO: add other keys");
        } else {
            key.chars().next().unwrap()
        };

        if action == "noop" {
            self.remove(mode, &Key::Char(key));
            return Ok(());
        }

        let action = match action.parse() {
            Ok(a) => a,
            Err(action::Error::NotAnAction) => {
                return Err("not an action: {action}\r");
            }
        };

        self.map(mode, Key::Char(key), action)
            .ok_or("Failed to map")
    }

    pub fn parse_group(
        &mut self,
        line: &str,
        lines: &mut dyn Iterator<Item = &str>,
        line_num: &mut usize,
    ) {
        let name = line
            .chars()
            .take_while(|b| ![' ', '{'].contains(b))
            .collect::<String>();

        if !self.mode_exists(&name) {
            eprintln!("Unknown mode {name}\r");
            return;
        }

        *line_num += 1;
        while let Some(line) = lines.next().filter(|l| !l.contains('}')) {
            *line_num += 1;
            match self.parse_line(&name, line.trim()) {
                Ok(()) => (),
                Err("no action") => {
                    eprintln!("no action on line {}\r", line_num);
                }
                Err("No such mode") => {
                    unreachable!();
                }
                Err(e) => {
                    eprintln!("error \"{e}\" on line {}\r", line_num);
                }
            }
        }
        *line_num += 1;
    }
}

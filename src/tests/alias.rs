use crate::config::Config;

#[test]
fn empty_alias() {
    let mut conf = Config::default();
    conf.parse_lines(vec!["alias:", "no: ne:te"]);
    assert_eq!(conf.aliases.len(), 2);
    assert_eq!(conf.aliases["alias"], "");
    assert_eq!(conf.aliases["no"], "ne:te");

    assert_eq!(conf.alias("alias").unwrap(), (String::new(), Vec::new()))
}

#[test]
fn alias_recursive() {
    let mut conf = Config::default();
    conf.parse_lines(vec!["ls:ls --color=auto", "la:ls -a"]);

    assert_eq!(conf.aliases["ls"], "ls --color=auto");

    assert_eq!(
        conf.alias("la").unwrap(),
        ("ls".into(), vec!["--color=auto".into(), "-a".into()])
    );
}

#[test]
fn alias_alias() {
    let mut conf = Config::default();
    conf.parse_lines(vec![
        "ls:exa",
        "la:ls -a",
        "ll:ls -l",
        "lla:ll -a",
        "llam:lla -m",
    ]);

    assert_eq!(
        conf.alias("llam").unwrap(),
        ("exa".into(), vec!["-l".into(), "-a".into(), "-m".into()])
    );
}

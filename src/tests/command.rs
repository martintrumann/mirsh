use std::io;

use crate::command::Command;

#[test]
fn simple_command() -> Result<(), io::Error> {
    let cmd = Command::from("ls");

    assert!(cmd.execute().is_ok());
    Ok(())
}

#[test]
fn unknown_command() -> Result<(), io::Error> {
    let cmd = Command::from("thiscommanddoesntexists");

    assert!(cmd.execute().is_err());
    Ok(())
}

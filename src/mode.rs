use std::fmt;
use termion::{color, cursor};

pub struct Mode {
    pub letter: char,
    pub colors: String,
    pub cursor: Cursor,
}

impl fmt::Display for Mode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}{}{}{}",
            self.colors,
            self.letter,
            color::Bg(color::Reset),
            color::Fg(color::Reset),
            self.cursor
        )
    }
}

pub enum Cursor {
    Bar,
    Block,
    Underline,
}

impl fmt::Display for Cursor {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Cursor::Bar => write!(f, "{}", cursor::SteadyBar),
            Cursor::Block => write!(f, "{}", cursor::SteadyBlock),
            Cursor::Underline => write!(f, "{}", cursor::SteadyUnderline),
        }
    }
}

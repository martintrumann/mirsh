use std::{collections::HashMap, env};

use crate::Shell;

type BuiltinFn = for<'a, 'b> fn(&'a mut Shell, &'b [String]);

pub fn builtins() -> HashMap<&'static str, BuiltinFn> {
    let mut builtins = HashMap::<_, BuiltinFn>::new();
    builtins.insert("cd", change_current_dir);
    builtins.insert("history", history);
    builtins.insert("quit", quit);
    builtins.insert("ck", comp_kill);
    builtins
}

fn change_current_dir(sh: &mut Shell, args: &[String]) {
    let home = env::var("HOME").ok();

    if let Some(dir) = args.first().or(home.as_ref()) {
        if let Err(e) = env::set_current_dir(dir) {
            eprintln!("{e}\r");
        } else {
            sh.cwd = env::current_dir().unwrap()
        }
    }
}

fn comp_kill(s: &mut Shell, _args: &[String]) {
    for comp in s.completions.iter_mut() {
        comp.kill()
    }
}

fn history(s: &mut Shell, _: &[String]) {
    for line in s.history.get() {
        println!("{}\r", line);
    }
}

fn quit(s: &mut Shell, _: &[String]) {
    s.run = false;
}
